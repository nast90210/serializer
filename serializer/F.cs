using System;

namespace serializer
{
    [Serializable]
    public class F
    {
        public int i1, i2, i3, i4;
        public string i5;

        public static F Get() => new F(){ i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = "бла" };
    }
}