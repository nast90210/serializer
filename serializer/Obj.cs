using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace serializer
{
    public class Obj
    {
        public static async Task<string> SerializeAsync<T>(T obj)
        {
            var result = Path.GetTempFileName();

            await Task.Run(() =>
            {
                Dictionary<string, string> temp = new Dictionary<string, string>();

                foreach (var field in typeof(T).GetTypeInfo().DeclaredFields)
                {
                    temp.Add(field.Name, field.GetValue(obj)?.ToString());
                }

                File.AppendAllText(result, String.Join(";", temp.Keys));
                File.AppendAllText(result, Environment.NewLine);
                File.AppendAllText(result, String.Join(";", temp.Values));

            });            
            return result;
        }

        public static async Task<T> DesirializeAsync<T>(string path) where T : new()
        {
            var result = new T();

            await Task.Run(() =>
            {
                var lines = File.ReadAllLines(path);
                
                var keys = lines[0].Split(";");
                var values = lines[1].Split(";");

                for (int i = 0; i < keys.Length; i++)
                {
                    var field = typeof(T).GetTypeInfo().GetDeclaredField(keys[i]);
                    var filedType = field?.FieldType;
                    field?.SetValue(result, Convert.ChangeType(values[i],filedType!));
                }
            });
            
            return result;
        }
        
        public static string Serialize<T>(T obj)
        {
            var result = Path.GetTempFileName();


                Dictionary<string, string> temp = new Dictionary<string, string>();

                foreach (var field in typeof(T).GetTypeInfo().DeclaredFields)
                {
                    temp.Add(field.Name, field.GetValue(obj)?.ToString());
                }

                File.AppendAllText(result, String.Join(";", temp.Keys));
                File.AppendAllText(result, Environment.NewLine);
                File.AppendAllText(result, String.Join(";", temp.Values));

          
            return result;
        }

        public static T Desirialize<T>(string path) where T : new()
        {
            var result = new T();

            
                var lines = File.ReadAllLines(path);
                
                var keys = lines[0].Split(";");
                var values = lines[1].Split(";");

                for (int i = 0; i < keys.Length; i++)
                {
                    var field = typeof(T).GetTypeInfo().GetDeclaredField(keys[i]);
                    var filedType = field?.FieldType;
                    field?.SetValue(result, Convert.ChangeType(values[i],filedType!));
                }

                return result;
        }
    }
}